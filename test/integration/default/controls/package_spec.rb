# frozen_string_literal: true

packages = %w[
  cdparanoia
  flac
  glyr
  imagemagick
  lame
  opus-tools
  vorbis-tools
  vorbisgain
]

packages.each do |pkg|
  control "abcde-package-install-pkg-#{pkg}-installed" do
    title 'should be installed'
    describe package(pkg) do
      it { should be_installed }
    end
  end
end
