# frozen_string_literal: true

packages = %w[
  cdparanoia
  flac
  glyr
  imagemagick
  lame
  opus-tools
  vorbis-tools
  vorbisgain
]

packages.each do |pkg|
  control "abcde-package-install-pkg-#{pkg}-removed" do
    title 'should not be installed'
    describe package(pkg) do
      it { should_not be_installed }
    end
  end
end
