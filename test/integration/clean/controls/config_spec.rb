# frozen_string_literal: true

control 'abcde-config-clean-zsh-rc-includes-config-auser-absent' do
  title 'should not exist'

  describe file('/home/auser/.config/zsh/rc_includes/abcde.zsh') do
    it { should_not exist }
  end
end

control 'abcde-config-clean-abcde-config-auser-absent' do
  title 'should not exist'

  describe file('/home/auser/.config/abcde/abcde.conf') do
    it { should_not exist }
  end
end

control 'abcde-config-clean-abcde-dir-auser-absent' do
  title 'should not exist'

  describe directory('/home/auser/.config/abcde') do
    it { should_not exist }
  end
end
