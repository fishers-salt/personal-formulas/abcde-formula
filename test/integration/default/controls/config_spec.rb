# frozen_string_literal: true

control 'abcde-config-file-user-auser-present' do
  title 'should be present'

  describe user('auser') do
    it { should exist }
  end
end

control 'abcde-config-file-abcde-dir-auser-managed' do
  title 'should exist'

  describe directory('/home/auser/.config/abcde') do
    it { should be_owned_by 'auser' }
    it { should be_grouped_into 'auser' }
    its('mode') { should cmp '0755' }
  end
end

control 'abcde-config-file-output-dir-auser-managed' do
  title 'should exist'

  describe directory('/home/auser/music/incoming') do
    it { should be_owned_by 'auser' }
    it { should be_grouped_into 'auser' }
    its('mode') { should cmp '0755' }
  end
end

control 'abcde-config-file-abcde-config-auser-managed' do
  title 'should match desired lines'

  describe file('/home/auser/.config/abcde/abcde.conf') do
    it { should be_file }
    it { should be_owned_by 'auser' }
    it { should be_grouped_into 'auser' }
    its('mode') { should cmp '0644' }
    its('content') { should include('# Your changes will be overwritten.') }
    its('content') { should include('OUTPUTDIR=/home/auser/music/incoming') }
    its('content') { should include('OUTPUTTYPE="mp3"') }
  end
end

control 'abcde-config-file-zsh-rc-include-dir-auser-managed' do
  title 'should exist'

  describe directory('/home/auser/.config/zsh/rc_includes') do
    it { should be_owned_by 'auser' }
    it { should be_grouped_into 'auser' }
    its('mode') { should cmp '0755' }
  end
end

control 'abcde-config-file-zsh-rc-includes-config-auser-managed' do
  title 'should match desired lines'

  describe file('/home/auser/.config/zsh/rc_includes/abcde.zsh') do
    it { should be_file }
    it { should be_owned_by 'auser' }
    it { should be_grouped_into 'auser' }
    its('mode') { should cmp '0644' }
    its('content') { should include('# Your changes will be overwritten.') }
    its('content') do
      should include('alias abcde=\'abcde -c $HOME/.config/abcde/abcde.conf\'')
    end
  end
end
