# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- set sls_package_install = tplroot ~ '.package.install' %}
{%- from tplroot ~ "/map.jinja" import mapdata as abcde with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}

include:
  - {{ sls_package_install }}

{% if salt['pillar.get']('abcde-formula:use_users_formula', False) %}

{% for name, user in pillar.get('users', {}).items() if user.absent is not defined or not user.absent %}
{%- set current = salt.user.info(name) -%}
{%- if user == None -%}
{%- set user = {} -%}
{%- endif -%}
{%- set home = user.get('home', current.get('home', "/home/%s" % name)) -%}
{%- set manage = user.get('manage_abcde', False) -%}
{%- if 'prime_group' in user and 'name' in user['prime_group'] %}
{%- set user_group = user.prime_group.name -%}
{%- else -%}
{%- set user_group = name -%}
{%- endif %}
{%- if manage -%}

abcde-config-file-user-{{ name }}-present:
  user.present:
    - name: {{ name }}

abcde-config-file-abcde-dir-{{ name }}-managed:
  file.directory:
    - name: {{ home }}/.config/abcde
    - user: {{ name }}
    - group: {{ user_group }}
    - mode: '0755'
    - makedirs: True
    - require:
      - abcde-config-file-user-{{ name }}-present

{%- set user_abcde_pillar = user.get('abcde', {}) -%}

{%- set output_dir = user_abcde_pillar.get('output_dir', None) %}
{%- if output_dir == None %}
{%- set output_dir = abcde.output_dir %}
{%- endif %}

abcde-config-file-output-dir-{{ name }}-managed:
  file.directory:
    - name: {{ output_dir }}
    - user: {{ name }}
    - group: {{ user_group }}
    - mode: '0755'
    - makedirs: True
    - require:
      - abcde-config-file-user-{{ name }}-present

{%- set output_formats = user_abcde_pillar.get('output_formats', None) %}
{%- if output_formats == None %}
{%- set output_formats = abcde.output_formats %}
{%- endif %}
abcde-config-file-config-{{ name }}-managed:
  file.managed:
    - name: {{ home }}/.config/abcde/abcde.conf
    - source: {{ files_switch([
                  name ~ '-abcde.conf.tmpl',
                  'abcde.conf.tmpl'],
                lookup='abcde-config-file-config-managed',
                )
              }}
    - mode: '0644'
    - user: {{ name }}
    - group: {{ user_group }}
    - template: jinja
    - context:
        output_formats: {{ output_formats }}
        output_dir: {{ output_dir }}
    - require:
      - abcde-config-file-abcde-dir-{{ name }}-managed

abcde-config-file-zsh-rc-include-dir-{{ name }}-managed:
  file.directory:
    - name: {{ home }}/.config/zsh/rc_includes
    - user: {{ name }}
    - group: {{ user_group }}
    - mode: '0755'
    - makedirs: True
    - require:
      - abcde-config-file-user-{{ name }}-present

abcde-config-file-zsh-rc-includes-config-{{ name }}-managed:
  file.managed:
    - name: {{ home }}/.config/zsh/rc_includes/abcde.zsh
    - source: {{ files_switch([
                  name ~ '-abcde_rc.zsh.tmpl',
                  'abcde_rc.zsh.tmpl'],
                lookup='abcde-config-file-zsh-rc-includes-config-' ~ name ~ '-managed',
                )
              }}
    - mode: '0644'
    - user: {{ name }}
    - group: {{ user_group }}
    - template: jinja
    - require:
      - abcde-config-file-zsh-rc-include-dir-{{ name }}-managed

{% endfor %}

{% endif %}
{% endfor %}
{% endif %}
